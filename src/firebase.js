import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

 // Your web app's Firebase configuration
 var firebaseConfig = {
    apiKey: "AIzaSyCXvv_9sG4zRNdg10TJSEgbt5ETq56eE38",
    authDomain: "reactslackchat.firebaseapp.com",
    databaseURL: "https://reactslackchat.firebaseio.com",
    projectId: "reactslackchat",
    storageBucket: "reactslackchat.appspot.com",
    messagingSenderId: "174443398243",
    appId: "1:174443398243:web:00f887a3bce2765e"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;